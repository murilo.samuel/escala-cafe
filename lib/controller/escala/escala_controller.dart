import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:escala_cafe_app/model/cafezeiro.dart';
import 'package:escala_cafe_app/model/escala.dart';
import 'package:escala_cafe_app/model/pessoa.dart';

class EscalaController {

  Future<Escala> getEscalaAtual() async {

    Query escalassQuery = await Firestore.instance.collection("escalas")
                                                  .orderBy("inicio", descending: true);

    QuerySnapshot escalas   = await escalassQuery.getDocuments();
    Escala escalaVigente = Escala.fromDocument(escalas.documents.first);

    var cafezeiros = await Firestore.instance .collection("escalas")
                                              .document(escalaVigente.id)
                                              .collection("cafezeiros").orderBy("data")
                                              .getDocuments();

    var ontem = DateTime.now().subtract(Duration(days: 1));

    for(DocumentSnapshot doc in cafezeiros.documents){
      var c = Cafezeiro.fromDocument(doc);
      if (c.data.isBefore(ontem)){
        continue;
      }
      var pessoa = await Firestore.instance.collection("pessoas").document(c.pessoaReference.documentID).get();
      c.pessoa = Pessoa.fromDocument(pessoa);
      escalaVigente.cafezeiros.add(c);
    }

    return escalaVigente;
  }

}