import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:escala_cafe_app/model/geral.dart';

class GeralController {

  Future<Geral> getGeral() async {

    DocumentSnapshot doc = await Firestore.instance.collection("geral").document("0").get();

    return Geral.fromDocument(doc);
  }

}