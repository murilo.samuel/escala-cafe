import 'package:cloud_firestore/cloud_firestore.dart';

class Pessoa {

  String id;
  String nome;

  Pessoa.fromDocument(DocumentSnapshot snapshot){
    id = snapshot.documentID;
    nome = snapshot.data["nome"];
  }

}