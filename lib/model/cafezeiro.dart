import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:escala_cafe_app/model/pessoa.dart';

class Cafezeiro {

  String id;
  DateTime data;
  DocumentReference pessoaReference;
  Pessoa pessoa;

  Cafezeiro.fromDocument(DocumentSnapshot snapshot){
    this.id = snapshot.documentID;
    this.pessoaReference = snapshot.data["pessoa"];
    Timestamp dataTimiestamp = snapshot.data["data"];
    this.data = DateTime.fromMillisecondsSinceEpoch(dataTimiestamp.millisecondsSinceEpoch);
  }

}