import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:escala_cafe_app/model/produto.dart';

class Estoque {

  String id;
  Produto produto;
  DocumentReference produtoReference;
  int quantidade;

  Estoque.fromDocument(DocumentSnapshot snapshot){
    id = snapshot.documentID;
    quantidade = snapshot.data["quantidade"];
    produtoReference = snapshot.data["produto"];
  }

}