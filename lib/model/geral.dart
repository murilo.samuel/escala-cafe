import 'package:cloud_firestore/cloud_firestore.dart';

class Geral {

  String id;
  double saldo;
  bool emManutencao;

  Geral.fromDocument(DocumentSnapshot snapshot){
    this.id = snapshot.documentID;
    this.emManutencao = snapshot.data["em_manutencao"];
    try {
      this.saldo = snapshot.data["saldo"];
    } catch (Exception){
      int saldoInt = snapshot.data["saldo"];
      this.saldo = saldoInt.toDouble();
    }
  }

}