import 'package:escala_cafe_app/model/enumerators/dia_semana.dart';

class DiaSemanaHelper {

  static EDiaSemana getDiaSemana(int dia){

    switch(dia){
      case 1 :
        return EDiaSemana.segunda;
      case 2 :
        return EDiaSemana.terca;
      case 3 :
        return EDiaSemana.quarta;
      case 4 :
        return EDiaSemana.quinta;
      case 5 :
        return EDiaSemana.sexta;
      case 6 :
        return EDiaSemana.sabado;
      case 7 :
        return EDiaSemana.domingo;
      default:
        return null;
    }

  }

  static String getAbreviacao(EDiaSemana dia){

    switch(dia) {
      case EDiaSemana.segunda:
        return "SEG";
      case EDiaSemana.terca:
        return "TER";
      case EDiaSemana.quarta:
        return "QUA";
      case EDiaSemana.quinta:
        return "QUI";
      case EDiaSemana.sexta:
        return "SEX";
      case EDiaSemana.sabado:
        return "SAB";
      case EDiaSemana.domingo:
        return "DOM";
      default:
        return "XXX";
    }

  }

}