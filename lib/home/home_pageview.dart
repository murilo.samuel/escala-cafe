import 'package:escala_cafe_app/home/home_screen.dart';
import 'package:flutter/widgets.dart';

class HomePageView extends StatefulWidget {
  @override
  _HomePageViewState createState() => _HomePageViewState();
}

class _HomePageViewState extends State<HomePageView> {

  final _pageController = PageController();

  @override
  Widget build(BuildContext context) {

    return PageView(
      physics: NeverScrollableScrollPhysics(),
      controller: _pageController,
      children: <Widget>[
        HomeScreen()
      ],
    );
  }
}
