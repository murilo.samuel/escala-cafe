import 'dart:async';

import 'package:escala_cafe_app/controller/escala/escala_controller.dart';
import 'package:escala_cafe_app/controller/estoque/estoque_controller.dart';
import 'package:escala_cafe_app/controller/geral/geral_controller.dart';
import 'package:escala_cafe_app/controller/pagamento/pagamento_controller.dart';
import 'package:escala_cafe_app/model/cafezeiro.dart';
import 'package:escala_cafe_app/model/enumerators/dia_semana.dart';
import 'package:escala_cafe_app/model/enumerators/dia_semana_helper.dart';
import 'package:escala_cafe_app/model/escala.dart';
import 'package:escala_cafe_app/model/estoque.dart';
import 'package:escala_cafe_app/model/geral.dart';
import 'package:escala_cafe_app/model/pagamento.dart';
import 'package:escala_cafe_app/model/participante_pagamento.dart';
import 'package:escala_cafe_app/util/AppColors.dart';
import 'package:escala_cafe_app/util/date_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:intl/intl.dart';

class HomeScreen extends StatefulWidget {

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  final numberFormater = new NumberFormat("#,##0.00", "pt_BR");
  
  FlutterLocalNotificationsPlugin notificationsPlugin;

  EstoqueController   estoqueController   = EstoqueController();
  PagamentoController pagamentoController = PagamentoController();
  EscalaController    escalaController    = EscalaController();
  GeralController     geralController     = GeralController();

  Future<Escala>        _escalaFuture;
  Future<List<Estoque>> _estoqueFuture;
  Future<Pagamento>     _pagamentoFuture;
  Future<Geral>         _geralFuture;

  String labelPagamento = "Pagamento";

  @override
  void initState() {
    
    super.initState();
    
    _escalaFuture     = escalaController.getEscalaAtual();
    _estoqueFuture    = estoqueController.getEstoque();
    _pagamentoFuture  = pagamentoController.getPagamentoParticipantes();
    _geralFuture      = geralController.getGeral();
    
    /*var settingsAndroid = new AndroidInitializationSettings('@mipmap/ic_launcher');
    var settingsIOS     = new IOSInitializationSettings();
    var settings        = new InitializationSettings(settingsAndroid, settingsIOS);

    notificationsPlugin = new FlutterLocalNotificationsPlugin();
    notificationsPlugin.initialize(settings, onSelectNotification: onSelectNotification);*/
    
  }

  Future onSelectNotification(String payload) async {
    showDialog(
      context: context,
      builder: (_) => new AlertDialog(
        title: const Text("Teste1"),
        content: Text("Teste2 $payload"),
      )
    );
  }

  Future _showNotificationWithSound() async {
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        'your channel id', 'your channel name', 'your channel description',
        sound: 'slow_spring_board',
        importance: Importance.Max,
        priority: Priority.High);
    var iOSPlatformChannelSpecifics =
    new IOSNotificationDetails(sound: "slow_spring_board.aiff");
    var platformChannelSpecifics = new NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    print("scheduled");
    await notificationsPlugin.showDailyAtTime(
      0,
      'New Post',
      'How to Show Notification in Flutter',
      new Time(23,45,0),
      platformChannelSpecifics,
      payload: 'Custom_Sound',
    );
  }
// Method 2
  Future _showNotificationWithDefaultSound() async {
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        'your channel id', 'your channel name', 'your channel description',
        importance: Importance.Max, priority: Priority.High);
    var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
    var platformChannelSpecifics = new NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await notificationsPlugin.show(
      0,
      'New Post',
      'How to Show Notification in Flutter',
      platformChannelSpecifics,
      payload: 'Default_Sound',
    );
  }
// Method 3
  Future _showNotificationWithoutSound() async {
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        'your channel id', 'your channel name', 'your channel description',
        playSound: false, importance: Importance.Max, priority: Priority.High);
    var iOSPlatformChannelSpecifics =
    new IOSNotificationDetails(presentSound: false);
    var platformChannelSpecifics = new NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await notificationsPlugin.show(
      0,
      'New Post',
      'How to Show Notification in Flutter',
      platformChannelSpecifics,
      payload: 'No_Sound',
    );
  }

  @override
  Widget build(BuildContext context) {

    Widget _bodyBack() => Container(
      color: AppColors.primaryColor,
    );

    return Material(
      child: Stack(
        children: <Widget>[

          _bodyBack(),

          RefreshIndicator(
            onRefresh: atualizar,
            child: CustomScrollView(

              slivers: <Widget>[

                _appBar(),

                _escalaList(),

                _separator(),

                _estoqueList(),

                _separator(),

                _pagamentoList(),

                _separator(),

                _saldoEmCaixa(),

                _separator(),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _escalaList() {

    return SliverToBoxAdapter(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[

            _getTitleContainter("Escala"),

            FutureBuilder<Escala>(
              future: _escalaFuture,
              builder: (context, snapshot){

                if (!snapshot.hasData) {
                  return _getProgress(AppColors.tercearyColor);
                } else {

                  return Container(
                    height: 145,
                    child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: snapshot.data.cafezeiros.length,
                        itemBuilder: (context, index) {
                          return _escalaCard(snapshot.data.cafezeiros[index]);
                        }
                    ),
                  );
                }
              },
            ),
          ],
        )
    );
  }

  Widget _estoqueList() {

    return SliverToBoxAdapter(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[

            _getTitleContainter("Estoque"),

            FutureBuilder<List<Estoque>>(
              future: _estoqueFuture,
              builder: (context, snapshot){
                if (!snapshot.hasData) {
                  return _getProgress(AppColors.tercearyColor);
                } else {

                  return Container(
                    height: 150,
                    color: AppColors.tercearyColor,
                    child: ListView.builder(
                        padding: EdgeInsets.only(left: 5.0),
                        scrollDirection: Axis.horizontal,
                        itemCount: snapshot.data.length,
                        itemBuilder: (context, index) {
                          return _estoqueCard(snapshot.data[index]);
                        }
                    ),
                  );
                }
              },
            ),
          ],
        )
    );
  }

  Widget _pagamentoList() {

    return SliverToBoxAdapter(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[

            _getTitleContainter(labelPagamento),

            FutureBuilder<Pagamento>(
              future: _pagamentoFuture,
              builder: (context, snapshot){
                if (!snapshot.hasData) {
                  return _getProgress(AppColors.tercearyColor);
                } else {

                  return Container(
                    height: 160,
                    color: AppColors.tercearyColor,
                    child: ListView.builder(
                        padding: EdgeInsets.only(left: 5.0),
                        scrollDirection: Axis.horizontal,
                        itemCount: snapshot.data.participantes.length,
                        itemBuilder: (context, index) {
                          return _pagamentoCard(snapshot.data, snapshot.data.participantes[index]);
                        }
                    ),
                  );
                }
              },
            ),

          ],
        )
    );
  }

  Widget _escalaCard(Cafezeiro cafezeiro){

    bool passou = cafezeiro.data.isBefore(DateTime.now());
    String data = DateHelper.convertToDateString(cafezeiro.data.millisecondsSinceEpoch);
    bool hoje = data == DateHelper.convertToDateString(DateTime.now().millisecondsSinceEpoch);
    EDiaSemana diaSemanaEnum = DiaSemanaHelper.getDiaSemana(cafezeiro.data.weekday);
    String diaSemana = DiaSemanaHelper.getAbreviacao(diaSemanaEnum);
    Color corTexto = hoje ? AppColors.primaryColor : AppColors.secondaryColor;
    corTexto = passou ? Colors.grey : corTexto;
    Color corCard = hoje ? AppColors.secondaryColor : AppColors.primaryColor;

    return Container(
      width: 150.0,
      color: AppColors.tercearyColor,
      child: GestureDetector(
        child: Card(
          margin: EdgeInsets.symmetric(horizontal: 8.0, vertical: 16.0),
          elevation: 10.0,
          color: corCard,
          child: Column(
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Column(
                    children: <Widget>[
                      Text(data, style : TextStyle(fontSize: 17, color: corTexto, fontWeight: FontWeight.bold)),
                      Text(diaSemana, style : TextStyle(fontSize: 40, color: corTexto, fontWeight: FontWeight.bold)),
                      Text("${cafezeiro.pessoa.nome}", style: TextStyle(color: Colors.white, fontSize: 22),)
                    ],
                  )
              ),

            ],
          ),
        ),
      ),
    );

  }

  Widget _estoqueCard(Estoque estoque){

    return Container(
      width: 165.0,
      color: AppColors.tercearyColor,
      child: GestureDetector(
        onTap: (){
        },
        child: Card(
          margin: EdgeInsets.symmetric(horizontal: 8.0, vertical: 22.0),
          elevation: 10.0,
          color: AppColors.primaryColor,
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 15.0),
                child: Image(image: new AssetImage("assets/images/${estoque.produto.icone}.png"), width: 50,),
              ),
              Padding(
                  padding: EdgeInsets.only(top: 10.0),
                  child: Text("${estoque.quantidade} ${estoque.produto.unidadeMedida} ${estoque.produto.nome}", style: TextStyle(color: Colors.white, fontSize: 18),)
              ),
            ],
          ),
        ),
      ),
    );

  }

  Widget _pagamentoCard(Pagamento pagamento, ParticipantePagamento participantePagamento){

    Color corCard = participantePagamento.pagou ? AppColors.secondaryColor : AppColors.primaryColor;
    Color corTexto = participantePagamento.pagou ? AppColors.primaryColor : AppColors.secondaryColor;
    var iconPagou = participantePagamento.pagou ? Icons.attach_money : Icons.assistant_photo;

    Future.delayed(Duration.zero, () => setState(() {
      labelPagamento = "Pagamento - " + DateHelper.convertToDateString(pagamento.inicio.millisecondsSinceEpoch, format: "MM/yyyy");
    }));

    return Container(
      width: 165.0,
      color: AppColors.tercearyColor,
      child: GestureDetector(
        child: Card(
          margin: EdgeInsets.symmetric(horizontal: 8.0, vertical: 22.0),
          elevation: 10.0,
          color: corCard,
          child: Column(
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(top: 15.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(iconPagou, size: 40, color: corTexto),
                      Text("R\$${numberFormater.format(participantePagamento.valor)}",
                        style: TextStyle(color: corTexto,
                            fontSize: 25,
                            fontWeight:
                            FontWeight.bold),)
                    ],
                  )
              ),
              Padding(
                  padding: EdgeInsets.only(top: 5.0),
                  child: Text("${participantePagamento.pessoa.nome}", style: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold),)
              ),
            ],
          ),
        ),
      ),
    );

  }

  Widget _saldoEmCaixa() {

    return SliverToBoxAdapter(
        child: Container(
            margin: EdgeInsets.symmetric(vertical: 30, horizontal: 5),
            color: AppColors.primaryColor,
            child: Column(
              children: <Widget>[

                FutureBuilder<Geral>(
                  future: _geralFuture,
                  builder: (context, snapshot){
                    if (!snapshot.hasData) {
                      return _getProgress(AppColors.primaryColor);
                    } else {

                      return Text("R\$${numberFormater.format(snapshot.data.saldo)}",
                              style: TextStyle(
                              color: AppColors.secondaryColor,
                              fontSize: 50,
                              fontWeight: FontWeight.bold),
                      );
                    }
                  },
                ),

                Text("em caixa",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 25,
                      fontWeight: FontWeight.bold),
                )
              ],
            )
        )
    );
  }

  Widget _getTitleContainter(String text){
    return Container(
        color: AppColors.tercearyColor,
        child: Padding(
          padding: EdgeInsets.only(left: 12.0, top: 15.0),
          child: Text("$text", style: TextStyle(color: AppColors.primaryColor, fontWeight: FontWeight.bold, fontSize: 20),),
        )
    );
  }

  Widget _separator() {

    return SliverToBoxAdapter(
      child: Container(height: 30,),
    );
  }

  Widget _appBar() {

    return SliverAppBar(
      floating: true,
      snap: true,
      backgroundColor: Colors.transparent,
      elevation: 0.0,
      flexibleSpace: FlexibleSpaceBar(
        title: const Text("HOME", style: TextStyle(fontSize: 30),),
        centerTitle: true,
      ),
    );
  }

  Widget _getProgress(Color backColor){
    return Container(
      color: backColor,
      height: 150,
      alignment: Alignment.center,
      child: CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(AppColors.secondaryColor),
      ),
    );
  }

  Future<Null> atualizar() async  {

    setState(() {
      _escalaFuture    = Future.value();
      _estoqueFuture   = Future.value();
      _pagamentoFuture = Future.value();
      _geralFuture     = Future.value();
    });

    Completer<Null> completer = Completer<Null>();

    atualizaDadosFuture().then((atualizado){
      completer.complete();
    });

    return null;
  }

  Future<bool> atualizaDadosFuture() async {

    Completer<bool> completer = Completer<bool>();

    atualizarDados().then((atualizado){
      completer.complete(atualizado);
    });


    return completer.future;

  }

  Future<bool> atualizarDados() async {

    var escala = Future.value(await escalaController.getEscalaAtual());
    setState(() {
      _escalaFuture = escala;
    });

    var estoque = Future.value(await estoqueController.getEstoque());
    setState(() {
      _estoqueFuture = estoque;
    });

    var pagamento = Future.value(await pagamentoController.getPagamentoParticipantes());
    setState(() {
      _pagamentoFuture = pagamento;
    });

    var geral = Future.value(await geralController.getGeral());
    setState(() {
      _geralFuture = geral;
    });

    return true;
  }

}