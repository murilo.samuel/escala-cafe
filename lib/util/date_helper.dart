import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';

class DateHelper {
  static DateTime convertToDate(int timestamp){
    return new DateTime.fromMillisecondsSinceEpoch(timestamp);
  }

  static String convertToDateString(int timestamp, {String format}){

    if (timestamp == null){
      return "";
    }

    var date = convertToDate(timestamp);
    initializeDateFormatting("pt_BR", null);
    final f = new DateFormat(format == null || format.isEmpty ? 'dd/MM/yy': format);

    return f.format(date);
  }
}